#!/usr/bin/env python3

import pandas as pd
import xml.etree.ElementTree as ET
import urllib.request
import sys
import os

def create_dir(dirname):
    if not os.path.exists(dirname):
        os.mkdir(dirname)
        
def download_file(url, file_name):
    urllib.request.urlretrieve(url, file_name)
    

def parse_bt_xml(file_name):
    tree = ET.parse(file_name)
    doc_root = tree.getroot()
    
    wahlperiode = [x  for x in doc_root.iter('wahlperiode')][0].text
    of_date = [x  for x in doc_root.iter('datum')][0].get('date')

    tops = [x for x in doc_root.iter('tagesordnungspunkt')]

    top_dict = {}
    for top in tops:
        top_id = top.get('top-id')
        speeches = [x for x in top.iter('rede')]

        top_dict[top_id] = {}
        for speech in speeches:
            speaker = [x for x in speech.iter('redner')][0]
            speaker_id = speaker.get('id')
            sp_name = [x.text for x in speaker.iter('nachname')][0]
            sp_givenname = [x.text for x in speaker.iter('vorname')][0]
            sp_id = speech.get('id')

            top_dict[top_id][sp_id] = {
                'name': sp_name,
                'given_name': sp_givenname,
                'speaker_id': speaker_id
            }

            nJ = 0
            text = []
            for par in speech.iter('p'):
                par_class = par.get('klasse')            

                if par_class in ['J_1', 'J']:
                    if par_class == 'J_1':
                        nJ += 1

                    if nJ < 2:
                        tmp = par.text
                        tmp = tmp.replace(u'\xa0', u' ')
                        tmp = tmp.replace(u'\u202f', u'')
                        text.append( tmp )
            text = ' '.join(text)
            top_dict[top_id][sp_id]['conten'] = text

    out = []

    for key, value in top_dict.items():
        tmp = pd.DataFrame(value).T
        tmp['top'] = key
        tmp['wahlperiode'] = wahlperiode
        tmp['date'] = of_date
        out.append(tmp)
    
    return( pd.concat(out) )

def main():
    args = sys.argv

    url = args[1]

    url_parts = url.split('/')
    file_name = url_parts[ len(url_parts)-1 ]

    create_dir('data')
    create_dir('data/xml')
    create_dir('data/csv')
    
    xml_file_path =  f'data/xml/{file_name}'
    download_file(url, xml_file_path)
    
    df = parse_bt_xml(xml_file_path)
    
    df = df[['name','given_name','speaker_id','conten','top','wahlperiode','date']].copy()
    
    df.to_csv(
        'data/csv/' + file_name.split('.')[0] + '.csv'
    )
    
    
if __name__ == '__main__':
    main()